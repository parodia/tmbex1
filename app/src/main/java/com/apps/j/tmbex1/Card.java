package com.apps.j.tmbex1;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by j on 25.03.15.
 */
public class Card implements Comparable<Card>{
    int id;
    CardNumber number;
    CardSymbol symbol;
    CardShading shading;
    CardColor color;

    public Card(int id, CardNumber number, CardSymbol symbol, CardShading shading, CardColor color) {
        this.id = id;
        this.number = number;
        this.symbol = symbol;
        this.shading = shading;
        this.color = color;
    }

    public Drawable getDrawable(Context context) {
        String cardName;
        if (id<10)
            cardName = "0"+id+".gif";
        else cardName = id+".gif";

        InputStream ims = null;
        try {
            ims = context.getAssets().open(cardName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Drawable.createFromStream(ims, null);
    }

    public int compareCards(Card card1, Card card2) {
        int result = 0;
        if ((this.number == card1.number && this.number == card2.number) || (this.number != card1.number && this.number != card2.number && card1.number != card2.number)) result++;
        if ((this.symbol == card1.symbol && this.symbol == card2.symbol) || (this.symbol != card1.symbol && this.symbol != card2.symbol && card1.symbol != card2.symbol)) result++;
        if ((this.shading == card1.shading && this.shading == card2.shading) || (this.shading != card1.shading && this.shading != card2.shading && card1.shading != card2.shading)) result++;
        if ((this.color == card1.color && this.color == card2.color) || (this.color != card1.color && this.color != card2.color && card1.color != card2.color)) result++;
        return result;
    }

    @Override
    public String toString() {
        return id+"";//+" "+number+" "+symbol+" "+shading;
    }

    @Override
    public int compareTo(Card another) {
        Integer i = this.id;
        return i.compareTo(another.id);
    }
}

enum CardNumber {
    ONE,
    TWO,
    THREE
}

enum CardSymbol {
    DIAMOND,
    SQUIGGLE,
    OVAL
}

enum CardShading {
    SOLID,
    STRIPED,
    OPEN
}

enum CardColor {
    RED,
    GREEN,
    VIOLET
}
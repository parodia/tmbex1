package com.apps.j.tmbex1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class MainActivity extends Activity {
    int WINNING_FACTOR = 3;
    List<Card> allCards;
    List<Card> cards;
    List<List<Card>> sets;
    List<List<Card>> foundSets;
    TextView textView;
    List<ImageButton> buttons;
    List<Card> selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        Button button = (Button)findViewById(R.id.button);

        allCards = new ArrayList<>();
        setCards();

        buttons = new ArrayList<>();
        for (int i=0; i<12; i++) {
            int resID = getResources().getIdentifier("imageButton"+(i+1), "id", getPackageName());
            ImageButton imageButton = (ImageButton)findViewById(resID);
            buttons.add(imageButton);
        }

        String description = "The deck consists of 81 cards. Your task is to collect three cards which vary by only one feature (number, symbol, shading, color). Counter shows how many opportunities you have.";

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(description)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        randomizeCards();
                    }
                })
                .setCancelable(false)
                .show();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("button pressed", "NEW CARDS");
                randomizeCards();
            }
        });

        final Context context = this;
        for (final ImageButton imageButton: buttons) {
            imageButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Card card = cards.get(buttons.indexOf(imageButton));
                    if (!selected.contains(card)) {
                        if(selected.size() < 3) {
                            selected.add(card);
                            imageButton.setAlpha(0.2f);
                        } else
                            Toast.makeText(getApplicationContext(), "Cannot select more than three cards", Toast.LENGTH_SHORT).show();
                    } else {
                        selected.remove(card);
                        imageButton.setAlpha(1f);
                    }
                    Collections.sort(selected);
                    Log.i("selected",""+selected);
                    if(selected.size() == 3) {
                        if (sets.contains(selected)) {
                            if (!foundSets.contains(selected)) {
                                Toast.makeText(getApplicationContext(), "GREAT!", Toast.LENGTH_SHORT).show();
                                foundSets.add(selected);
                                textView.setText(foundSets.size() + "/" + sets.size());
                                Log.i("result","GREAT!");
                            } else {
                                Toast.makeText(getApplicationContext(), "Already found", Toast.LENGTH_SHORT).show();
                            }
                            selected = new ArrayList<>();
                            for (ImageButton imageButton1: buttons)
                                imageButton1.setAlpha(1f);
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "Only " + selected.get(0).compareCards(selected.get(1), selected.get(2)) + " common features!", Toast.LENGTH_SHORT).show();
                        }
                        if (foundSets.size() == sets.size()) {
                            //Toast.makeText(getApplicationContext(), "You won!!!", Toast.LENGTH_SHORT).show();
                            for (ImageButton imageButton1: buttons) {
                                imageButton1.setEnabled(false);
                                imageButton1.setAlpha(0.2f);
                            }
                            Log.i("result","You won!!!");

                            AlertDialog.Builder builder = new AlertDialog.Builder(context);
                            builder.setMessage("Want to play once again?")
                                    .setTitle("You won!")
                                    .setPositiveButton("Yes!", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            randomizeCards();
                                        }
                                    })
                                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            MainActivity.this.finish();
                                        }
                                    })
                                    .setCancelable(false)
                                    .show();
                        }
                    }
                }
            });
        }


    }


    private void setCards() {
        allCards.add(new Card(1, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(2, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(3, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(4, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(5, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(6, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(7, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(8, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(9, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.SOLID, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(10, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(11, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(12, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(13, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(14, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(15, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(16, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(17, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(18, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.SOLID, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(19, CardNumber.ONE, CardSymbol.OVAL, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(20, CardNumber.TWO, CardSymbol.OVAL, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(21, CardNumber.THREE, CardSymbol.OVAL, CardShading.SOLID, CardColor.RED));
        allCards.add(new Card(22, CardNumber.ONE, CardSymbol.OVAL, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(23, CardNumber.TWO, CardSymbol.OVAL, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(24, CardNumber.THREE, CardSymbol.OVAL, CardShading.SOLID, CardColor.VIOLET));
        allCards.add(new Card(25, CardNumber.ONE, CardSymbol.OVAL, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(26, CardNumber.TWO, CardSymbol.OVAL, CardShading.SOLID, CardColor.GREEN));
        allCards.add(new Card(27, CardNumber.THREE, CardSymbol.OVAL, CardShading.SOLID, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(28, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(29, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(30, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(31, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(32, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(33, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(34, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(35, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(36, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.STRIPED, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(37, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(38, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(39, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(40, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(41, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(42, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(43, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(44, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(45, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.STRIPED, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(46, CardNumber.ONE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(47, CardNumber.TWO, CardSymbol.OVAL, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(48, CardNumber.THREE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.RED));
        allCards.add(new Card(49, CardNumber.ONE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(50, CardNumber.TWO, CardSymbol.OVAL, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(51, CardNumber.THREE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.VIOLET));
        allCards.add(new Card(52, CardNumber.ONE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(53, CardNumber.TWO, CardSymbol.OVAL, CardShading.STRIPED, CardColor.GREEN));
        allCards.add(new Card(54, CardNumber.THREE, CardSymbol.OVAL, CardShading.STRIPED, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(55, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(56, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(57, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(58, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(59, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(60, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(61, CardNumber.ONE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(62, CardNumber.TWO, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(63, CardNumber.THREE, CardSymbol.SQUIGGLE, CardShading.OPEN, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(64, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(65, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(66, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(67, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(68, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(69, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(70, CardNumber.ONE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(71, CardNumber.TWO, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(72, CardNumber.THREE, CardSymbol.DIAMOND, CardShading.OPEN, CardColor.GREEN));
        //////////////////////////////////////////////////////////////
        allCards.add(new Card(73, CardNumber.ONE, CardSymbol.OVAL, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(74, CardNumber.TWO, CardSymbol.OVAL, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(75, CardNumber.THREE, CardSymbol.OVAL, CardShading.OPEN, CardColor.RED));
        allCards.add(new Card(76, CardNumber.ONE, CardSymbol.OVAL, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(77, CardNumber.TWO, CardSymbol.OVAL, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(78, CardNumber.THREE, CardSymbol.OVAL, CardShading.OPEN, CardColor.VIOLET));
        allCards.add(new Card(79, CardNumber.ONE, CardSymbol.OVAL, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(80, CardNumber.TWO, CardSymbol.OVAL, CardShading.OPEN, CardColor.GREEN));
        allCards.add(new Card(81, CardNumber.THREE, CardSymbol.OVAL, CardShading.OPEN, CardColor.GREEN));
    }

    private List<List<Card>> getCombinations(List<Card> list) {
        List<List<Card>> sets = new ArrayList<>();
        for (int i=0; i<list.size(); i++)
            for (int j=i+1; j<list.size(); j++)
                for (int k=j+1; k<list.size(); k++) {
                    Card card1 = list.get(i);
                    Card card2 = list.get(j);
                    Card card3 = list.get(k);
                    if (card1.compareCards(card2, card3) == 4) {
                        sets.add(Arrays.asList(card1, card2, card3));
                    }
                }
        return sets;
    }

    private void randomizeCards() {
        Random random = new Random();
        cards = new ArrayList<>();
        while (cards.size() < 12) {
            Card card = allCards.get(random.nextInt(81));
            if (!cards.contains(card))
                cards.add(card);
        }
        Collections.sort(cards);
        Log.i("cards", cards + "");

        sets = getCombinations(cards);
        Log.i("sets",sets+"");
        Log.i("sets", sets.size() + " sets");

        if (sets.size() < 3)
            randomizeCards();
        else {
            textView.setText("0/"+sets.size());

            for (ImageButton imageButton: buttons) {
                imageButton.setImageDrawable(cards.get(buttons.indexOf(imageButton)).getDrawable(getApplicationContext()));
                imageButton.setEnabled(true);
                imageButton.setAlpha(1f);
            }
            selected = new ArrayList<>();
            foundSets = new ArrayList<>();
        }
    }

}
